# CSCI-654 Project 2

Individual Project #2

Use the following command to run LargestTriangleMPI locally on my ubuntu vm:

```mpiexec java -cp csci-654-project-2.jar edu.rit.cs.LargestTriangleMPI 100 100 142857```

Use the following command to copy the jar to docker00:

```cd ~/cs654/Project2/csci-654-project-2/target```

```scp csci-654-project-2.jar docker00.cs.rit.edu:~/csci654/csci-654-project-2/target/.```

Note: I have setup docker00 with my public ssh key.

On docker00, use the following commands to START my docker containers:

```ech6581@docker00:~$ docker container start container-ech6581-1```
```ech6581@docker00:~$ docker container start container-ech6581-2```
```ech6581@docker00:~$ docker container start container-ech6581-3```


On docker00, use the following command to deploy the jar to the docker containers:

```cd ~/csci654/csci-654-project-2```

```bash deploy.bash```

On docker00, use the following command to attach to container-ech6581-1:

```docker attach container-ech6581-1```

On container-ech6581-1, use the following command to run LargestTriangleMPI:

```mpiexec --allow-run-as-root --hostfile myHostfile java -cp csci-654-project-2.jar edu.rit.cs.LargestTriangleMPI 100 100 142857```

On docker00, use the following commands to STOP my docker containers:

```ech6581@docker00:~$ docker container stop container-ech6581-1```
```ech6581@docker00:~$ docker container stop container-ech6581-2```
```ech6581@docker00:~$ docker container stop container-ech6581-3```

On tardis, use the following command to run LargestTriangleMPI:

```cd ~/csci654/csci-654-project-2/target```

```mpirun --hostfile /usr/local/pub/ph/TardisCluster3Nodes.txt --prefix /usr/local java -cp csci-654-project-2.jar edu.rit.cs.LargestTriangleMPI 100 100 142857```

```mpirun --hostfile /usr/local/pub/ph/TardisCluster3Nodes.txt --prefix /usr/local java -cp csci-654-project-2.jar edu.rit.cs.LargestTriangleMPI 3000 100 142857```