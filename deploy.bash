#!/bin/bash

# Deploy to each of our docker containers
docker cp ./target/csci-654-project-2.jar container-ech6581-1:/csci654
docker cp ./target/csci-654-project-2.jar container-ech6581-2:/csci654
docker cp ./target/csci-654-project-2.jar container-ech6581-3:/csci654

# Deploy our run script to each of our docker containers
#docker cp run.bash container-ech6581-1:/csci654
#docker cp run.bash container-ech6581-2:/csci654
#docker cp run.bash container-ech6581-3:/csci654
