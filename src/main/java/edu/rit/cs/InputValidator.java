package edu.rit.cs;

public class InputValidator {
    public static int N = 0;
    public static double side = 0;
    public static long seed = 0;

    public static boolean validate(String[] args) {

        // Check that 3 arguments are present
        if(args.length != 3) {
            System.out.println("ERRPR: Need to specify 3 commandline arguments.");
            return false;
        }

        // Check that arg[0] can be parsed into a valid int
        try {
            N = Integer.parseInt(args[0]);
        } catch (Exception e) {
            System.out.println("ERROR: first argument must be a valid integer.");
            return false;
        }

        // Check that arg[0] is positive and greater than 0.
        if(N <= 0) {
            System.out.println("ERROR: first argument must be greater than 0.");
            return false;
        }

        // Check that arg[1] can be parsed into a valid double
        try {
            side = Double.parseDouble(args[1]);
        } catch (Exception e) {
            System.out.println("ERROR: second argument must be a valid double.");
            return false;
        }

        // Check that arg[1] is positive
        if(side <= 0) {
            System.out.println("ERROR: second argument must be greater than 0.");
            return false;
        }

        // Check that arg[2] can be parsed into a valid long
        try {
            seed = Long.parseLong(args[2]);
        } catch (Exception e) {
            System.out.println("ERROR: third argument must be a valid long.");
            return false;
        }

        return true;
    }
}
