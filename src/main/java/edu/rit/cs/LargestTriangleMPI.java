package edu.rit.cs;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;

/**
 * LargestTriangleMPI
 *
 * Multi-node Parallel Solution implementation for the Largest Triangle problem.
 * Sequential version can be found in LargestTriangleSeq.
 *
 * @author Eric Hartman (ech6581@rit.edu)
 */
public class LargestTriangleMPI {
    private int N;
    private double side;
    private long seed;

    // MPI Tags for sending Triangles from Slaves to Master
    private static int TRIANGLE = 99;
    private static int TRIANGLE_END = 98;

    /**
     * Constructs the LargestTriangleMPI object
     *
     * This is the multi-node parallel version of the solution for
     * finding the largest triangle among a set of random
     * points.
     *
     * @param N The number of points to randomly generate
     * @param side The length of a side of the square region containing points
     * @param seed The seed value for the random number generator
     */
    public LargestTriangleMPI(int N, double side, long seed) {
        this.N = N;
        this.side = side;
        this.seed = seed;
    }

    /**
     * cachePoints
     *
     *   Method to pre-compute the randomized points as specified
     *   by the class N, side, and seed parameters.
     *
     * @return List<Point> containing a list of randomized points
     */
    private List<Point> cachePoints() {
        // Create a container for our Point objects
        List<Point> points = new ArrayList<Point>();

        // Create our point randomizer
        RandomPoints rnd = new RandomPoints(N, side, seed);

        // Add a dummy zero position entry so that we can index into
        // the points ArrayList using 1-based indexing instead of 0-based indexing
        points.add(new Point(-1,-1));

        // Cache the random points provided by point randomizer
        while (rnd.hasNext()) {
            points.add(rnd.next());
        }

        return points;
    }

    /**
     * euclideanDistance
     *
     * Calculates the euclidean distance between the given
     * pair of points.
     *
     * @param a first point
     * @param b second point
     * @return euclidean distance between first and second point
     */
    private double euclideanDistance(Point a, Point b) {
        // side length = sqrt( (p2.x - p1.x)^2 + (p2.y - p1.y)^2 )
        double xDelta = b.getX() - a.getX();
        double yDelta = b.getY() - a.getY();
        return Math.sqrt( (xDelta) * (xDelta) + (yDelta) * (yDelta) );
    }

    /**
     * calculateArea
     *
     * Calculates the area of the triangle based upon
     * the given points A, B, and C.
     *
     * @param a first Point
     * @param b second Point
     * @param c third Point
     * @return area of the given triangle
     */
    private double calculateArea(Point a, Point b, Point c) {
        // Calculate length of sides of the triangle: A,B,C
        double A = euclideanDistance(a, b);
        double B = euclideanDistance(b, c);
        double C = euclideanDistance(c, a);

        //System.out.println("distances=(" + A + ", " + B + ", " + C + ")");

        // s = (A + B +C) / 2
        double s = (A + B + C) / (double) 2;

        // Calculate the area of the triangle
        return Math.sqrt(s * (s - A)*(s - B)*(s - C));
    }

    /**
     * mergeIntoLargestTriangles
     *
     * Method to merge the given triangle into the largestTriangles list iff
     * the triangle's area is greater than or equal to the current largest area
     *
     * @param largestTriangles a list of Triangle objects that have equivalent areas, and are largest so far
     * @param triangle a candidate triangle
     */
    private void mergeIntoLargestTriangles(List<Triangle> largestTriangles, Triangle triangle) {
        if(largestTriangles.size() > 0) {
            if (triangle.area > largestTriangles.get(0).area) {
                // triangle is the new largest triangle so far!
                largestTriangles.clear();
                largestTriangles.add(triangle);
            } else if(triangle.area == largestTriangles.get(0).area) {
                // triangle is tied with current largest triangles so far!
                largestTriangles.add(triangle);
            }

            return;
        }
    }

    /**
     * sendTriangle
     *
     * Method to use MPI to send the serialized triangle.
     *
     * @param triangle a triangle to be sent to the master
     * @param lastOne flag to indicate if this is the last triangle to be sent by this process.
     */
    private void sendTriangle(Triangle triangle, boolean lastOne) {
        byte[] message = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(triangle);
            oos.flush();
            message = baos.toByteArray();
        }
        catch (Exception e) {
            System.out.println("ERROR: Could not serialize the triangle.");
            System.out.println(e);
        }

        try {
            if (lastOne) {
                //System.out.println("slave sending TRIANGLE_END with area=" + triangle.area);
                MPI.COMM_WORLD.send(message, message.length, MPI.BYTE, 0, TRIANGLE_END);
            } else {
                //System.out.println("slave sending TRIANGLE with area=" + triangle.area);
                MPI.COMM_WORLD.send(message, message.length, MPI.BYTE, 0, TRIANGLE);
            }
        } catch (MPIException e) {
            System.out.println("ERROR: exception when sending triangle!");
        }
    }

    /**
     * receiveTriangleAndMergeIntoLargestTriangles
     *
     * Method to use MPI to receive a triangle from a slave node and merge the received
     * triangle into the largest triangles list as appropriate.
     * When a triangle is received, the tag is inspected to determine if this is the
     * a triangle in a sequence or a triangle at the end of a sequence.  If the triangle
     * is the end of a sequence, the method will return true.
     *
     * @param largestTriangles the list of largest triangles
     * @return returns true if this is the last triangle in a sequence, false otherwise.
     */
    private boolean receiveTriangleAndMergeIntoLargestTriangles(List<Triangle> largestTriangles) {
        boolean lastOne = false;
        byte[] message = new byte[2000];

        try {
            //System.out.println("master receiving TRIANGLE or TRIANGLE_END");
            Status status = MPI.COMM_WORLD.recv(message, 2000, MPI.BYTE, MPI.ANY_SOURCE, MPI.ANY_TAG);

            if (status.getTag() == TRIANGLE) {
                //System.out.println("master received TRIANGLE");
            } else if (status.getTag() == TRIANGLE_END) {
                //System.out.println("master received TRIANGLE_END");
                lastOne = true;
            }
        } catch (MPIException e) {
            System.out.println("ERROR: MPI exception during triangle receive!");
            return true;
        }

        Triangle triangle;
        ByteArrayInputStream bis = new ByteArrayInputStream(message);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            Object obj = in.readObject();
            triangle = (Triangle) obj;

            // Add the triangle that we received to our largestTriangles array list...
            //System.out.println("master received triangle with area==" + triangle.area + ", lastOne=" + lastOne);
            mergeIntoLargestTriangles(largestTriangles, triangle);
        } catch (IOException ex) {
            System.out.println("ERROR: IO exception when deserializing message.");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR: class not found when deserializing message.");
        }

        return lastOne;
    }

    /**
     * performTriangleCommunications
     *
     * Method to perform all of the send/receive communications regarding the largest
     * triangles between the slaves and the master.
     *
     * @param rank This process' rank
     * @param size The number of processes
     * @param largestTriangles a list of largest triangles for the given master or slave
     */
    private void performTriangleCommunications(int rank, int size, List<Triangle> largestTriangles) {
        // If not rank0, send triangles to rank0
        if(rank != 0) {
            // Send our LargestTriangle to head node, only need to send first from our sorted List!
            sendTriangle(largestTriangles.get(0), true);

            // The code below would be used if there was interest in sending more than one triangle
            // from the slaves to the master.  This is not necessary.
            /*for(Triangle triangle: largestTriangles) {
                boolean lastTriangle = largestTriangles.indexOf(triangle) == (largestTriangles.size()-1);
                sendTriangle(triangle, lastTriangle);
            }*/
        } else {
            // accept winning triangles from other ranks and add to local largestTriangles array
            int triangleEndCount = 0;
            while(triangleEndCount < (size - 1)) {
                if(true == receiveTriangleAndMergeIntoLargestTriangles(largestTriangles)) {
                    // Received the last in a sequence of triangles
                    triangleEndCount++;
                } else {
                    // Received a non-terminating triangle!
                }
            }
        }
    }

    /**
     * calculateLargest
     *
     * Performs the work to determine which set of points
     * represent the largest triangle.
     *
     * Outputs the answer to the console.
     */
    private void calculateLargest() throws MPIException {
        // Cache the points
        List<Point> points = cachePoints();

        /*// Print out the cached points
        for(int j = 1; j < points.size(); j++) {
            System.out.println((j)+": x=" + points.get(j).getX() + " y=" + points.get(j).getY());
        }
        System.out.println("End of cached points");*/

        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        String hostname = MPI.getProcessorName();
        System.out.println("Rank=" + rank + ", size=" + size + ", running @ hostname=" + hostname);

        // Track largest triangle(s) so far
        double largestArea = 0;
        List<Triangle> largestTriangles = new ArrayList<Triangle>();

        // Iterate over the points for point1
        // For this outer-most loop, we will iterate by starting at 1 offset by rank
        // and then we will increment according to the world size.  This will ensure
        // that each node only processes a non-overlapping subset of the random points
        for(int index1 = rank + 1; index1 < points.size(); index1 += size) {
            // Iterate over the points for point2
            for (int index2 = index1 + 1; index2 < points.size(); index2++) {
                // Iterate over the points for point3
                for(int index3 = index2 + 1; index3 < points.size(); index3++) {
                    // Not checking for overlap of points for point1/point2/point3
                    // because if any point overlaps, then the area of the triangle
                    // would be 0, since triangle would really be a line or single point
                    double area = calculateArea(points.get(index1), points.get(index2), points.get(index3));

                    // Keep largest so far...
                    if(area > largestArea) {
                        //System.out.println("largest area = " + area + ", indexes=(" + index1 +"," + index2 + "," + index3 + ")");
                        // Initialize our list of largest triangles...
                        largestArea = area;
                        largestTriangles.clear();
                        //largestTriangles.add(new Triangle(index1, points.get(index1), index2, points.get(index2), index3, points.get(index3), area));
                        largestTriangles.add(new Triangle(index1, index2, index3, area));
                    } else if(area == largestArea && area > 0) {
                        //System.out.println("matching area = " + area + ", indexes=(" + (index1) +"," + (index2) + "," + (index3) + ")");
                        // Merge triangle into our list of largest triangles...
                        //largestTriangles.add(new Triangle(index1, points.get(index1), index2, points.get(index2), index3, points.get(index3), area));
                        largestTriangles.add(new Triangle(index1, index2, index3, area));
                    }
                }
            }
        }

        // Sort the largestTriangles according to our winning criteria
        Collections.sort(largestTriangles);

        // Now that the largest triangles have been determined, lets perform all of the communications
        // to send Triangles from Slaves to Master.
        performTriangleCommunications(rank, size, largestTriangles);

        if(rank != 0) {
            // Slaves are done at thie point, return!
            return;
        }

        // Sort the largestTriangles according to our winning criteria
        Collections.sort(largestTriangles);
        //System.out.println("Candidate triangles=" + largestTriangles.size());

        // Determine the winning triangle
        Triangle winner = largestTriangles.get(0);

        // Print the winning triangle
        System.out.printf ("%d %.5g %.5g%n", winner.index1, points.get(winner.index1).getX(), points.get(winner.index1).getY());
        System.out.printf ("%d %.5g %.5g%n", winner.index2, points.get(winner.index2).getX(), points.get(winner.index2).getY());
        System.out.printf ("%d %.5g %.5g%n", winner.index3, points.get(winner.index3).getX(), points.get(winner.index3).getY());
        System.out.printf ("%.5g%n", winner.area);
    }

    /**
     * main
     *
     * Main entry point for the LargestTriangleMPI program.
     *
     * @param args 3 command line argument specifying
     *             number of points, length of a side, and seed
     */
    public static void main( String[] args ) throws MPIException {
        long startTime = System.currentTimeMillis();

        // Initialization for MPI
        MPI.Init(args);

        // Perform input validation and calculations
        if(InputValidator.validate(args)) {
            LargestTriangleMPI mpi = new LargestTriangleMPI(InputValidator.N, InputValidator.side, InputValidator.seed);
            mpi.calculateLargest();
        }

        long finishTime = System.currentTimeMillis();

        // Report our elapsed time
        if(MPI.COMM_WORLD.getRank() == 0) {
            long elapsedTime = finishTime - startTime;
            System.out.println("Elapsed time=" + (float) elapsedTime / 1000 + " seconds");
        }

        // Finalization for MPI
        MPI.COMM_WORLD.barrier();
        MPI.Finalize();
    }
}