package edu.rit.cs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * LargestTriangleSeq
 *
 * Sequential Solution implementation for the Largest Triangle problem.
 * Multi-node Parallel version can be found in LargestTriangleMPI.
 *
 * @author Eric Hartman (ech6581@rit.edu)
 */
public class LargestTriangleSeq {
    private int N;
    private double side;
    private long seed;

    /**
     * Constructs the LargestTriangleSeq object
     *
     * This is the sequential version of the solution for
     * finding the largest triangle among a set of random
     * points.
     *
     * @param N The number of points to randomly generate
     * @param side The length of a side of the square region containing points
     * @param seed The seed value for the random number generator
     */
    public LargestTriangleSeq(int N, double side, long seed) {
        this.N = N;
        this.side = side;
        this.seed = seed;
    }

    /**
     * cachePoints
     *
     *   Method to pre-compute the randomized points as specified
     *   by the class N, side, and seed parameters.
     *
     * @return List<Point> containing a list of randomized points
     */
    private List<Point> cachePoints() {
        // Create a container for our Point objects
        List<Point> points = new ArrayList<Point>();

        // Create our point randomizer
        RandomPoints rnd = new RandomPoints(N, side, seed);

        // Add a dummy zero position entry so that we can index into
        // the points ArrayList using 1-based indexing instead of 0-based indexing
        points.add(new Point(-1,-1));

        // Cache the random points provided by point randomizer
        while (rnd.hasNext()) {
            points.add(rnd.next());
        }

        return points;
    }

    /**
     * euclideanDistance
     *
     * Calculates the euclidean distance between the given
     * pair of points.
     *
     * @param a first point
     * @param b second point
     * @return euclidean distance between first and second point
     */
    private double euclideanDistance(Point a, Point b) {
        // side length = sqrt( (p2.x - p1.x)^2 + (p2.y - p1.y)^2 )
        double xDelta = b.getX() - a.getX();
        double yDelta = b.getY() - a.getY();
        return Math.sqrt( (xDelta) * (xDelta) + (yDelta) * (yDelta) );
    }

    /**
     * calculateArea
     *
     * Calculates the area of the triangle based upon
     * the given points A, B, and C.
     *
     * @param a first Point
     * @param b second Point
     * @param c third Point
     * @return area of the given triangle
     */
    private double calculateArea(Point a, Point b, Point c) {
        // Calculate length of sides of the triangle: A,B,C
        double A = euclideanDistance(a, b);
        double B = euclideanDistance(b, c);
        double C = euclideanDistance(c, a);

        //System.out.println("distances=(" + A + ", " + B + ", " + C + ")");

        // s = (A + B +C) / 2
        double s = (A + B + C) / (double) 2;

        // Calculate the area of the triangle
        return Math.sqrt(s * (s - A)*(s - B)*(s - C));
    }

    /**
     * calculateLargest
     *
     * Performs the work to determine which set of points
     * represent the largest triangle.
     *
     * Outputs the answer to the console.
     */
    private void calculateLargest() {
        // Cache the points
        List<Point> points = cachePoints();
        /*// Override points...
        points.clear();
        points.add(new Point(1,1));
        points.add(new Point(3, 3));
        points.add(new Point(1,3));
        points.add(new Point(3,1));
        points.add(new Point(3,4));
        points.add(new Point(1, 3.5));*/

        /*// Print out the cached points
        for(int j = 1; j < points.size(); j++) {
            System.out.println((j)+": x=" + points.get(j).getX() + " y=" + points.get(j).getY());
        }
        System.out.println("End of cached points");*/

        // Track largest triangle(s) so far
        double largestArea = 0;
        List<Triangle> largestTriangles = new ArrayList<Triangle>();

        // Iterate over the points for point1
        for(int index1 = 1; index1 < points.size(); index1++) {
            // Iterate over the points for point2
            for (int index2 = index1+1; index2 < points.size(); index2++) {
                // Iterate over the points for point3
                for(int index3 = index2+1; index3 < points.size(); index3++) {
                    // Not checking for overlap of points for point1/point2/point3
                    // because if any point overlaps, then the area of the triangle
                    // would be 0, since triangle would really be a line or single point
                    double area = calculateArea(points.get(index1), points.get(index2), points.get(index3));

                    // Keep largest so far...
                    if(area > largestArea) {
                        //System.out.println("largest area = " + area + ", indexes=(" + index1 +"," + index2 + "," + index3 + ")");
                        // Initialize our list of largest triangles...
                        largestArea = area;
                        largestTriangles.clear();
                        //largestTriangles.add(new Triangle(index1, points.get(index1), index2, points.get(index2), index3, points.get(index3), area));
                        largestTriangles.add(new Triangle(index1, index2, index3, area));
                    } else if(area == largestArea && area > 0) {
                        //System.out.println("matching area = " + area + ", indexes=(" + (index1) +"," + (index2) + "," + (index3) + ")");
                        // Merge triangle into our list of largest triangles...
                        //largestTriangles.add(new Triangle(index1, points.get(index1), index2, points.get(index2), index3, points.get(index3), area));
                        largestTriangles.add(new Triangle(index1, index2, index3, area));
                    }
                }
            }
        }

        // Sort the largestTriangles according to our winning criteria
        Collections.sort(largestTriangles);
        //System.out.println("Candidate triangles=" + largestTriangles.size());

        // Determine the winning triangle
        Triangle winner = largestTriangles.get(0);

        // Print the winning triangle
        System.out.printf ("%d %.5g %.5g%n", winner.index1, points.get(winner.index1).getX(), points.get(winner.index1).getY());
        System.out.printf ("%d %.5g %.5g%n", winner.index2, points.get(winner.index2).getX(), points.get(winner.index2).getY());
        System.out.printf ("%d %.5g %.5g%n", winner.index3, points.get(winner.index3).getX(), points.get(winner.index3).getY());
        System.out.printf ("%.5g%n", winner.area);

        // Manually calculate the area of the correct winner
        /*double realArea = calculateArea(points.get(9), points.get(12), points.get(80));
        winner = new Triangle(9, points.get(9), 12, points.get(12), 80, points.get(80), realArea);
        System.out.printf ("real: %d %.5g %.5g%n", winner.index1, winner.point1.getX(), winner.point1.getY());
        System.out.printf ("real: %d %.5g %.5g%n", winner.index2, winner.point2.getX(), winner.point2.getY());
        System.out.printf ("real: %d %.5g %.5g%n", winner.index3, winner.point3.getX(), winner.point3.getY());
        System.out.printf ("real: %.5g%n", winner.area);*/
    }

    /**
     * main
     *
     * Main entry point for the LargestTriangleSeq program.
     *
     * @param args 3 command line argument specifying
     *             number of points, length of a side, and seed
     */
    public static void main( String[] args ) {
        long startTime = System.currentTimeMillis();

        if(InputValidator.validate(args)) {
            LargestTriangleSeq seq = new LargestTriangleSeq(InputValidator.N, InputValidator.side, InputValidator.seed);
            seq.calculateLargest();
        }

        long elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println("Elapsed time=" + (float) elapsedTime / 1000 + " seconds");
    }
}
