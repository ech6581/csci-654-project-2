package edu.rit.cs;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Triangle class is an abstraction of the indexes and points
 *   that together comprise a "triangle".
 */
public class Triangle implements Comparable<Triangle>, Serializable {
    public int index1, index2, index3;
    //public Point point1, point2, point3;
    public double area;

    /**
     * Constructs a Triangle object, but internally re-orders
     * the given indexes and points so that they are stored
     * internally in ascending "index" order.
     *
     * @param index1 index of the first Point
     * //@param point1 the first Point
     * @param index2 index of the second Point
     * //@param point2 the second Point
     * @param index3 index of the third Point
     * //@param point3 the third Point
     * @param area the area of the triangle
     */
    public Triangle(int index1, //Point point1,
                    int index2, //Point point2,
                    int index3, //Point point3,
                    double area) {

        //System.out.println("Triangle()-before: index1=" + index1 + ", index2=" + index2 + ", index3=" + index3);

        // Store the points in "index" order from smallest to largest...
        // Since there are only 6 possibilities, we'll just hardcode
        // this sorting logic manually here:                        // Index order
        if(index1 <= index2 && index2 <= index3) {                  // 1,2,3
            reorderHelper(index1, index2, index3); //, point1, point2, point3);
        } else if(index1 <= index3 && index3 <= index2) {           // 1,3,2
            reorderHelper(index1, index3, index2); //, point1, point3, point2);
        } else if(index2 <= index1 && index1 <= index3) {           // 2,1,3
            reorderHelper(index2, index1, index3); //, point2, point1, point3);
        } else if(index2 <= index3 && index3 <= index1) {           // 2,3,1
            reorderHelper(index2, index3, index1); //, point2, point3, point1);
        } else if(index3 <= index1 && index1 <= index2) {           // 3,1,2
            reorderHelper(index3, index1, index2); //, point3, point1, point2);
        } else {                                                    // 3,2,1
            reorderHelper(index3, index2, index1); //, point3, point2, point1);
        }

        //System.out.println("Triangle()-after: index1=" + this.index1 + ", index2=" + this.index2 + ", index3=" + this.index3);
        this.area = area;
    }

    /**
     * Helper method to re-order the points
     * @param index1 index for new first point
     * @param index2 index for new second point
     * @param index3 index for new third point
     * //@param point1 new first point
     * //@param point2 new second point
     * //@param point3 new third point
     */
    private void reorderHelper(int index1, int index2, int index3) { //, Point point1, Point point2, Point point3) {
        this.index1 = index1;
        this.index2 = index2;
        this.index3 = index3;
        //this.point1 = point1;
        //this.point2 = point2;
        //this.point3 = point3;
    }

    @Override
    /**
     * compareTo
     *
     * Provides the baseline comparison mechanism to
     * determine an ordering between any two Triangles.  This
     * is needed in order to sort collections based upon the criteria
     * needed for our specification:  index1, index2, index3 in order.
     *
     * @param o is some other Triangle to be compared to this Triangle
     */
    public int compareTo(Triangle o) {
        if(this.index1 > o.index1)
            return 1;
        else if(this.index1 == o.index1) {
            // Need to check index2
            if(this.index2 > o.index2) {
                return 1;
            } else if(this.index2 == o.index2) {
                // Need to check index3
                if(this.index3 > o.index3) {
                    return 1;
                } else if(this.index3 == o.index3) {
                    return 0;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

}
